import React, { useState } from "react";
import "./App.css";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

import ModalWrapper from "./components/Modal/ModalWrapper";
import ModalHeader from "./components/Modal/ModalHeader";
import ModalFooter from "./components/Modal/ModalFooter";
import ModalBody from "./components/Modal/ModalBody";
import ModalClose from "./components/Modal/ModalClose";
import Image from "./components/Image/Image";
function App() {
  const user = "Daniel";
  const [isFirstModalOpen, setIsFirstModalOpen] = useState(false); // Стейт для первой модалки
  const [isSecondModalOpen, setIsSecondModalOpen] = useState(false); // Стейт для второй модалки

  const openFirstModal = () => {
    setIsFirstModalOpen(true);
    setIsSecondModalOpen(false); // Закрываем вторую модалку
  };

  // Функция для открытия второй модалки
  const openSecondModal = () => {
    setIsSecondModalOpen(true);
    setIsFirstModalOpen(false); // Закрываем первую модалку
  };

  // Функция для закрытия модальных окон
  const closeModal = () => {
    setIsFirstModalOpen(false);
    setIsSecondModalOpen(false);
  };

  return (
    <>
      <h1>Hello {user}</h1>
      {/* Первая кнопка для открытия первой модалки */}
      <Button onClick={openFirstModal}>Open first modal</Button>
      {/* Вторая кнопка для открытия второй модалки */}
      <Button onClick={openSecondModal}>Open second modal</Button>

      {/* Первая модалка */}
      <Modal isOpen={isFirstModalOpen}>
        <ModalWrapper>
          <ModalHeader>
            <ModalClose onClick={closeModal} />
            <Image></Image>
          </ModalHeader>

          <ModalBody>
            <h1>Product Delete!</h1>
            <p>
              By clicking the “Yes, Delete” button, PRODUCT NAME will be <br />
              deleted
            </p>
          </ModalBody>
          <ModalFooter
            firstText={"NO, CANCEL"}
            secondaryText={"YES, DELETE"}
            firstClick={closeModal}
          ></ModalFooter>
        </ModalWrapper>
      </Modal>

      {/* Вторая модалка */}
      <Modal isOpen={isSecondModalOpen}>
        <ModalWrapper>
          <ModalHeader>
            <ModalClose onClick={closeModal} />
          </ModalHeader>
          <ModalBody>
            <h1>Add Product “NAME”</h1>
            <p>Description for you product</p>
          </ModalBody>
          <ModalFooter firstText={"ADD TO FAVORITE"}></ModalFooter>
        </ModalWrapper>
      </Modal>
    </>
  );
}

export default App;
