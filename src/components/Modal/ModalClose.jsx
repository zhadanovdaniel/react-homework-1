export default function ModalClose({ onClick }) {
  return (
    <>
      <button class="ModalClose" onClick={onClick}>
        <img src="src\assets\closeCross.png" alt="" />
      </button>
    </>
  );
}
